package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Services.MeasureDetailService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
public class MeasureDetailController {

    @Inject
    MeasureDetailService measureDetailService;

    @GetMapping(value = "/measureDetails")
    public List<MeasureDetail> allMeasureDetails(){
        return measureDetailService.findAll();
    }

    @GetMapping(value = "/measureDetail/{id}")
    public MeasureDetail findById(@PathVariable(value = "id") Long id){
        MeasureDetail measureDetail = measureDetailService.findOne(id);
        return measureDetail;
    }

    @PutMapping(value = "/measureDetails")
    public MeasureDetail addMeasureDetail(@RequestBody MeasureDetail measureDetail){
        measureDetailService.addMeasure(measureDetail);
        return measureDetail;
    }

    @DeleteMapping(value = "/measureDetail/{id}")
    public void deleteMeasureDetail(@PathVariable(value = "id") Long id){
        MeasureDetail measureDetail = measureDetailService.findOne(id);
        measureDetailService.delete(measureDetail);
    }


}
