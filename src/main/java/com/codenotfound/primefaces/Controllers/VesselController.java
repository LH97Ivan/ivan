package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.GraphicInterval;
import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.DataDTO.*;
import com.codenotfound.primefaces.Services.*;
import com.codenotfound.primefaces.Views.GraphicView;
import com.codenotfound.primefaces.Views.MapView;
import com.codenotfound.primefaces.Views.VesselView;
import jdk.vm.ci.meta.Local;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.Circle;
import org.primefaces.model.map.LatLng;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class VesselController {

    @Inject
    VesselService vesselService;

    @Inject
    VesselView vesselView;

    @Inject
    MapView mapView;

    @Inject
    GraphicView graphicView;

    @Inject
    MeasureDetailService measureDetailService;

    @Inject
    MeasurePointService measurePointService;

    @Inject
    GraphicIntervalService graphicIntervalService;

    public void onVesselSelectInVesselView(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Vessel selected", vesselView.getSelectedVessel().getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        mapView.refreshMap(vesselView.getSelectedVessel());
        graphicView.getShowHideGraphic().clear();
        if(vesselView.getSelectedVessel().getPosX()!=null && vesselView.getSelectedVessel().getPosY()!=null){
            mapView.setCenterRevGeoMap(vesselView.getSelectedVessel().getPosX().toString() + "," + vesselView.getSelectedVessel().getPosY().toString());
        }else{
            mapView.setCenterRevGeoMap("36.140622, -5.396004");
        }
        addMeasuresToGraphicView(vesselView.getSelectedVessel().getId());
        addGraphicIntervalsToGraphicView(vesselView.getSelectedVessel().getId());
        graphicView.onGraphicSelectInGraphicsView();
    }

    public void onVesselSelectInMapView(OverlaySelectEvent event) {
        for(VesselDTO vessel: vesselView.getVessels()){
            LatLng center = new LatLng(vessel.getPosX(), vessel.getPosY());
            if(center.equals(((Circle)event.getOverlay()).getCenter())){
                mapView.refreshMap(vessel);
                vesselView.setSelectedVessel(vessel);
                addMeasuresToGraphicView(vessel.getId());
                addGraphicIntervalsToGraphicView(vessel.getId());
                graphicView.onGraphicSelectInGraphicsView();
                break;
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Circle Selected ", null));
    }

    public void onVesselUnselectInVesselView(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Vessel deselected");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        mapView.refreshMap(vesselView.getSelectedVessel());
        mapView.setCenterRevGeoMap("36.140622, -5.396004");
        graphicView.getMeasures().clear();
        graphicView.getOriginalMeasures().clear();
        graphicView.getMeasuresPlusThreeHours().clear();
        graphicView.getGraphicIntervalsPlusSixHours().clear();
    }

    public void onVesselSelectInGraphicsView(){
        VesselDTO selectedVessel;
        graphicView.getMeasures().clear();
        graphicView.getShowHideGraphic().clear();
        if(!vesselView.getSelectedImo().isEmpty()){
            selectedVessel = vesselService.findVesselByImo(vesselView.getSelectedImo());
            addMeasuresToGraphicView(selectedVessel.getId());
            addGraphicIntervalsToGraphicView(selectedVessel.getId());
            graphicView.onGraphicSelectInGraphicsView();
            for(MeasureDetailDTO measureDetailDTO: graphicView.getMeasures()){
                graphicView.getShowHideGraphic().put(measureDetailDTO.getMeasureIntervalName(), false);
            }
        }
    }

    public void addMeasuresToGraphicView (long vesselId) {
        List<MeasureDetailDTO> measures = new ArrayList<>();
        List<MeasureDetailDTO> originalMeasures = new ArrayList<>();
        List<MeasureDetailDTO> measuresPlusThreeHours = new ArrayList<>();
        List<MeasureDetailDTO> measuresPlusSixHours = new ArrayList<>();
        for(MeasureDetail measure: measureDetailService.findMeasuresByVesselId(vesselId)){
            MeasureDetailDTO measureDetailDTO = measureDetailService.getMeasureDetailDTOFromData(measure);
            measures.add(measureDetailDTO);
            MeasureDetailDTO originalMeasure = new MeasureDetailDTO(measureDetailDTO);
            originalMeasures.add(originalMeasure);
            LocalDateTime newEtaPlusThreeHours = measureDetailDTO.getEta().plusHours(3);
            LocalDateTime newEtdPlusThreeHours = measureDetailDTO.getEtd().plusHours(3);
            LocalDateTime newStartTimePlusThreeHours = measureDetailDTO.getStartTime().plusHours(3);
            LocalDateTime newEndTimePlusThreeHours = measureDetailDTO.getEndTime().plusHours(3);
            MeasureDetailDTO measurePlusThreeHours = new MeasureDetailDTO(measureDetailDTO);
            measurePlusThreeHours.setEta(newEtaPlusThreeHours);
            measurePlusThreeHours.setEtd(newEtdPlusThreeHours);
            measurePlusThreeHours.setStartTime(newStartTimePlusThreeHours);
            measurePlusThreeHours.setEndTime(newEndTimePlusThreeHours);
            measuresPlusThreeHours.add(measurePlusThreeHours);
            LocalDateTime newEtaPlusSixHours = measureDetailDTO.getEta().plusHours(6);
            LocalDateTime newEtdPlusSixHours = measureDetailDTO.getEtd().plusHours(6);
            LocalDateTime newStartTimePlusSixHours = measureDetailDTO.getStartTime().plusHours(6);
            LocalDateTime newEndTimePlusSixHours = measureDetailDTO.getEndTime().plusHours(6);
            MeasureDetailDTO measurePlusSixHours = new MeasureDetailDTO(measureDetailDTO);
            measurePlusSixHours.setEta(newEtaPlusSixHours);
            measurePlusSixHours.setEtd(newEtdPlusSixHours);
            measurePlusSixHours.setStartTime(newStartTimePlusSixHours);
            measurePlusSixHours.setEndTime(newEndTimePlusSixHours);
            measuresPlusSixHours.add(measurePlusSixHours);
        }
        graphicView.setMeasures(measures);
        graphicView.setOriginalMeasures(originalMeasures);
        graphicView.setMeasuresPlusThreeHours(measuresPlusThreeHours);
        graphicView.setMeasuresPlusSixHours(measuresPlusSixHours);
    }

    private void addGraphicIntervalsToGraphicView(Long vesselId) {
        List<GraphicIntervalDTO> res = new ArrayList<>();
        for(MeasureDetail measure: measureDetailService.findMeasuresByVesselId(vesselId)){
            MeasureDetailDTO measureDetailDTO = measureDetailService.getMeasureDetailDTOFromData(measure); //Aqui tengo un measure con su status propio
            GraphicInterval graphicInterval = graphicIntervalService.findByName(measureDetailDTO.getMeasureIntervalName());
            GraphicIntervalDTO graphicToProve = new GraphicIntervalDTO(graphicInterval); //Aqui tengo un intervalo en el que iremos cambiando el estado
            Integer graphicIndex = null; //Indice del elemento de la lista res
            for(int i=0;i<res.size();i++){ //for para obtener el indice del elemento de la lista que habría que modificar
                if(res.get(i).getName().equals(graphicToProve.getName())){ //Si en res hay un intervalo que coincide en nombre con el otro intervalo
                    graphicIndex=i; //Modificamos el indice del elemento para que apunte a este
                    break;
                }
            }
            if(graphicIndex==null){ //Si graphicIndex es null significa que graphicToProve no esta en la lista res
                graphicToProve.setStatus(measureDetailDTO.getStatusDTO()); //Cambiamos el estado del intervalo que se añadira a la lista final
                res.add(graphicToProve);
            } else if(res.get(graphicIndex).getStatus().equals("Yellow")){ //Si el intervalo en la lista tiene status Yellow
                if(measureDetailDTO.getStatusDTO().equals("Red")){ //Comprobamos si el estado del measure por individual tiene estado Red
                    graphicToProve.setStatus("Red"); //Si lo tiene, cambiamos el estado del intervalo en la lista final porque Red tiene prioridad
                    res.set(graphicIndex, graphicToProve);
                }
            } else if(res.get(graphicIndex).getStatus().equals("Green")){ //Si el intervalo en la lista tiene status Green
                if(measureDetailDTO.getStatusDTO().equals("Red")){ //Lo mismo que antes
                    graphicToProve.setStatus("Red");
                    res.set(graphicIndex, graphicToProve);
                } else if(measureDetailDTO.getStatusDTO().equals("Yellow")){
                    graphicToProve.setStatus("Yellow");
                    res.set(graphicIndex, graphicToProve);
                }
            }
        }
        graphicView.setGraphicIntervals(res);
        updateGraphicIntervalsToGraphicView(graphicView.getOriginalMeasures(), "original");
        updateGraphicIntervalsToGraphicView(graphicView.getMeasuresPlusThreeHours(), "three");
        updateGraphicIntervalsToGraphicView(graphicView.getMeasuresPlusSixHours(), "six");
    }

    //Este metodo es para actualizar los intervalos de las graficas que se le pasen
    private void updateGraphicIntervalsToGraphicView(List<MeasureDetailDTO> measures, String measuresType) {
        List<GraphicIntervalDTO> res = new ArrayList<>();
        for(MeasureDetailDTO measureDetailDTO: measures){
            GraphicInterval graphicInterval = graphicIntervalService.findByName(measureDetailDTO.getMeasureIntervalName());
            GraphicIntervalDTO graphicToProve = new GraphicIntervalDTO(graphicInterval); //Aqui tengo un intervalo en el que iremos cambiando el estado
            Integer graphicIndex = null; //Indice del elemento de la lista res
            for(int i=0;i<res.size();i++){ //for para obtener el indice del elemento de la lista que habría que modificar
                if(res.get(i).getName().equals(graphicToProve.getName())){ //Si en res hay un intervalo que coincide en nombre con el otro intervalo
                    graphicIndex=i; //Modificamos el indice del elemento para que apunte a este
                    break;
                }
            }
            if(graphicIndex==null){ //Si graphicIndex es null significa que graphicToProve no esta en la lista res
                graphicToProve.setStatus(measureDetailDTO.getStatusDTO()); //Cambiamos el estado del intervalo que se añadira a la lista final
                res.add(graphicToProve);
            } else if(res.get(graphicIndex).getStatus().equals("Yellow")){ //Si el intervalo en la lista tiene status Yellow
                if(measureDetailDTO.getStatusDTO().equals("Red")){ //Comprobamos si el estado del measure por individual tiene estado Red
                    graphicToProve.setStatus("Red"); //Si lo tiene, cambiamos el estado del intervalo en la lista final porque Red tiene prioridad
                    res.set(graphicIndex, graphicToProve);
                }
            } else if(res.get(graphicIndex).getStatus().equals("Green")){ //Si el intervalo en la lista tiene status Green
                if(measureDetailDTO.getStatusDTO().equals("Red")){ //Lo mismo que antes
                    graphicToProve.setStatus("Red");
                    res.set(graphicIndex, graphicToProve);
                } else if(measureDetailDTO.getStatusDTO().equals("Yellow")){
                    graphicToProve.setStatus("Yellow");
                    res.set(graphicIndex, graphicToProve);
                }
            }
        }
        if (measuresType.equals("edited")){
            graphicView.setGraphicIntervals(res);
        } else if(measuresType.equals("original")){
            graphicView.setOriginalGraphicIntervals(res);
        } else if(measuresType.equals("three")){
            graphicView.setGraphicIntervalsPlusThreeHours(res);
        } else if(measuresType.equals("six")){
            graphicView.setGraphicIntervalsPlusSixHours(res);
        }
    }

    public void applyNewETAAndETD(){
        for(MeasureDetailDTO measureDetailDTO: graphicView.getMeasures()){
            if(!vesselView.getSelectedVessel().getNewETA().isEmpty()){ //Para cambiar el valor de eta con el nuevo
                String[] time = vesselView.getSelectedVessel().getNewETA().split(":");
                int hour = Integer.parseInt(time[0].trim());
                int min = Integer.parseInt(time[1].trim());
                LocalDateTime newEta = measureDetailDTO.getEta().withHour(hour).withMinute(min);
                measureDetailDTO.setEta(newEta);
                vesselView.getSelectedVessel().setEta(newEta);
            }
            if(!vesselView.getSelectedVessel().getNewETD().isEmpty()){ //Para cambiar el valor de etd con el nuevo
                String[] time = vesselView.getSelectedVessel().getNewETD().split(":");
                int hour = Integer.parseInt(time[0].trim());
                int min = Integer.parseInt(time[1].trim());
                LocalDateTime newEtd = measureDetailDTO.getEtd().withHour(hour).withMinute(min);
                measureDetailDTO.setEtd(newEtd);
                vesselView.getSelectedVessel().setEtd(newEtd);
            }
            //Esto es para que se actualice el estado de la grafica, los puntos en el intervalo y lo demas
            MeasureDetail measureDetail = measureDetailService.findById(measureDetailDTO.getId());
            MeasureDetailDTO measureWithNewValues = measureDetailService.getMeasureDetailDTOFromDataChangingETAAndETD(measureDetail, measureDetailDTO.getEta(), measureDetailDTO.getEtd());
            measureDetailDTO.setStartTime(measureWithNewValues.getStartTime());
            measureDetailDTO.setEndTime(measureWithNewValues.getEndTime());
            measureDetailDTO.setStatusDTO(measureWithNewValues.getStatusDTO());
            updateGraphicIntervalsToGraphicView(graphicView.getMeasures(), "edited");
        }
        vesselView.getSelectedVessel().setNewETA("");
        vesselView.getSelectedVessel().setNewETD("");
    }
}
