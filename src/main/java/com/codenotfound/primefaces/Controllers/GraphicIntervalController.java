package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.GraphicInterval;
import com.codenotfound.primefaces.Services.GraphicIntervalService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
public class GraphicIntervalController {

    @Inject
    GraphicIntervalService graphicIntervalService;

    @GetMapping(value = "/graphicIntervals")
    public List<GraphicInterval> allGraphicIntervals(){
        return graphicIntervalService.findAll();
    }

    @GetMapping(value = "/graphicInterval/{id}")
    public GraphicInterval findById(@PathVariable(value = "id") Long id){
        GraphicInterval graphicInterval = graphicIntervalService.findOne(id);
        return graphicInterval;
    }
}
