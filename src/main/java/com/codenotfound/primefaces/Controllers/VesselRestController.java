package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.VesselData;
import com.codenotfound.primefaces.Services.MeasureDetailService;
import com.codenotfound.primefaces.Services.VesselService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
public class VesselRestController {

    @Inject
    VesselService vesselService;

    @Inject
    MeasureDetailService measureDetailService;

    @GetMapping(value = "/vessels")
    public List<VesselData> allVessels() {
        return vesselService.findAllVessels();
    }

    @GetMapping(value = "/vessel/{id}")
    public VesselData findById(@PathVariable(value = "id") Long id){
        VesselData vessel = vesselService.findOne(id);
        return vessel;
    }

    @PutMapping(value = "/vessels")
    public VesselData addVessel(@RequestBody VesselData vessel){
        vesselService.addVessel(vessel);
        return vessel;
    }

    @DeleteMapping(value = "/vessel/{id}")
    public void deleteVessel(@PathVariable(value = "id") Long id){
        VesselData vessel = vesselService.findOne(id);
        measureDetailService.deleteMeasuresDetailByVesselId(vessel.getId());
        vesselService.delete(vessel);
    }

}
