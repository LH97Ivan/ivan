package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.MeasurePoint;
import com.codenotfound.primefaces.Services.MeasurePointService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
public class MeasurePointController {

    @Inject
    MeasurePointService measurePointService;

    @GetMapping(value = "/measurePoints")
    public List<MeasurePoint> allMeasurePoints(){
        return measurePointService.findAll();
    }

    @GetMapping(value = "/measurePoint/{id}")
    public MeasurePoint findById(@PathVariable(value = "id") Long id){
        MeasurePoint measurePoint = measurePointService.findOne(id);
        return measurePoint;
    }

    @PutMapping(value = "/measurePoints")
    public MeasurePoint addMeasurePoint(@RequestBody MeasurePoint measurePoint){
        measurePointService.addMeasurePoint(measurePoint);
        return measurePoint;
    }

    @DeleteMapping(value = "/measurePoint/{id}")
    public void deleteMeasurePoint(@PathVariable(value = "id") Long id){
        MeasurePoint measurePoint = measurePointService.findOne(id);
        measurePointService.delete(measurePoint);
    }
}
