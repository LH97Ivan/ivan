package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.MeasureTypeData;
import com.codenotfound.primefaces.Services.MeasureTypeService;
import com.codenotfound.primefaces.Services.VesselService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.security.MessageDigest;
import java.util.List;

@RestController
public class MeasureTypeController {

    @Inject
    MeasureTypeService measureTypeService;

    @GetMapping(value = "/measureTypes")
    public List<MeasureTypeData> allMeasureTypes(){
        return measureTypeService.findAllMeasures();
    }

    @GetMapping(value = "/measureType/{id}")
    public MeasureTypeData findById(@PathVariable(value = "id") Long id){
        MeasureTypeData measure = measureTypeService.findOneMeasureById(id);
        return measure;
    }
}
