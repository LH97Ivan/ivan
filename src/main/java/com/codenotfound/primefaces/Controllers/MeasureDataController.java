package com.codenotfound.primefaces.Controllers;

import com.codenotfound.primefaces.Data.MeasureData;
import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Services.MeasureDataService;
import com.codenotfound.primefaces.Services.MeasureDetailService;
import com.codenotfound.primefaces.Services.MeasurePointService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
public class MeasureDataController {

    @Inject
    MeasureDataService measureDataService;

    @Inject
    MeasurePointService measurePointService;

    @Inject
    MeasureDetailService measureDetailService;

    @GetMapping(value = "/measureDatas")
    public List<MeasureData> allMeasureDatas(){
        return measureDataService.findAll();
    }

    @GetMapping(value = "/measureData/{id}")
    public MeasureData findById(@PathVariable(value = "id") Long id){
        MeasureData measureData = measureDataService.findOne(id);
        return measureData;
    }

    @PutMapping(value = "/measureDatas")
    public MeasureData addMeasureData(@RequestBody MeasureData measureData){
        measureDataService.addMeasureData(measureData);
        return measureData;
    }

    @DeleteMapping(value = "/measureData/{id}")
    public void deleteMeasureData(@PathVariable(value = "id") Long id){
        MeasureData measureData = measureDataService.findOne(id);
        measurePointService.deleteMeasurePointsByMeasureDataId(measureData.getId());
        measureDetailService.deleteMeasuresDetailByMeasureDataId(id);
        measureDataService.delete(measureData);
    }
}
