package com.codenotfound.primefaces.Views;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.DataDTO.GraphicIntervalDTO;
import com.codenotfound.primefaces.DataDTO.MeasureDetailDTO;
import com.codenotfound.primefaces.DataDTO.MeasurePointDTO;
import com.codenotfound.primefaces.Services.MeasureDetailService;
import com.codenotfound.primefaces.Services.MeasurePointService;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Named
@ViewScoped
public class GraphicView implements Serializable {


    private Map<String, List<Double>> values = new HashMap<>();

    private Map<String, String> times = new HashMap<>();

    private List<MeasureDetailDTO> measures = new ArrayList<>(); //measures a las que se les cambia el eta y el etd

    private List<MeasureDetailDTO> originalMeasures = new ArrayList<>(); //measures a las que no se les cambia el eta y el etd

    private List<MeasureDetailDTO> measuresPlusThreeHours = new ArrayList<>(); //measures con tres horas más de eta y etd

    private List<MeasureDetailDTO> measuresPlusSixHours = new ArrayList<>(); //measures con seis horas más de eta y etd

    private Map<String, Boolean> showHideGraphic = new HashMap<>();

    private List<GraphicIntervalDTO> graphicIntervals = new ArrayList<>(); //intervalos a los que se les cambia el eta y el etd

    private List<GraphicIntervalDTO> originalGraphicIntervals = new ArrayList<>(); //intervalos a los que no se les cambia el eta y el etd

    private List<GraphicIntervalDTO> graphicIntervalsPlusThreeHours = new ArrayList<>(); //intervalos con tres horas más de eta y etd

    private List<GraphicIntervalDTO> graphicIntervalsPlusSixHours = new ArrayList<>(); //intervalos con seis horas más de eta y etd

    private int measuresSize;

    @Inject
    MeasurePointService measurePointService;

    @Inject
    MeasureDetailService measureDetailService;

    @PostConstruct
    public void init(){
        refreshGraphics();
        showHideGraphic.clear();
    }

    public void refreshGraphics(){
        for(MeasureDetail measure: measureDetailService.findAll()) {
            MeasureDetailDTO measureDetailDTO = measureDetailService.getMeasureDetailDTOFromData(measure);
        }
    }

    public void generateGraphics(List<MeasureDetailDTO> measures){
        values.clear();
        times.clear();
        for(MeasureDetailDTO measure: measures){
            getPointsByMeasure(measure);
        }
    }

    private void getPointsByMeasure(MeasureDetailDTO measure) {
        List<MeasurePointDTO> points = measurePointService.findAllPointsByMeasureId(measure.getId());
        List<Double> valuesToAdd = new ArrayList<>();
        String timesToAdd = "";

        for(MeasurePointDTO point: points){
            valuesToAdd.add(point.getValue());
            if(!timesToAdd.equals("")){
                timesToAdd = timesToAdd + ",";
            }
            timesToAdd = timesToAdd +point.getTime().format(DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm", new Locale("es")));
        }

        values.put(measure.getMeasureTypeName(), valuesToAdd);
        times.put(measure.getMeasureTypeName(), timesToAdd);

    }

    public List<MeasureDetailDTO> getMeasures() {
        return measures;
    }

    public void setMeasures(List<MeasureDetailDTO> measures) {
        this.measures = measures;
    }

    public Map<String, List<Double>> getValues() {
        return values;
    }

    public Map<String, String> getTimes() {
        return times;
    }

    public Map<String, Boolean> getShowHideGraphic() {
        return showHideGraphic;
    }

    public void setShowHideGraphic(Map<String, Boolean> showHideGraphic) {
        this.showHideGraphic = showHideGraphic;
    }

    public List<GraphicIntervalDTO> getGraphicIntervals() {
        return graphicIntervals;
    }

    public void setGraphicIntervals(List<GraphicIntervalDTO> graphicIntervals) {
        this.graphicIntervals = graphicIntervals;
    }

    public int getMeasuresSize() {
        return measures.size();
    }

    public List<MeasureDetailDTO> getOriginalMeasures() {
        return originalMeasures;
    }

    public void setOriginalMeasures(List<MeasureDetailDTO> originalMeasures) {
        this.originalMeasures = originalMeasures;
    }

    public List<MeasureDetailDTO> getMeasuresPlusThreeHours() {
        return measuresPlusThreeHours;
    }

    public void setMeasuresPlusThreeHours(List<MeasureDetailDTO> measuresPlusThreeHours) {
        this.measuresPlusThreeHours = measuresPlusThreeHours;
    }

    public List<MeasureDetailDTO> getMeasuresPlusSixHours() {
        return measuresPlusSixHours;
    }

    public void setMeasuresPlusSixHours(List<MeasureDetailDTO> measuresPlusSixHours) {
        this.measuresPlusSixHours = measuresPlusSixHours;
    }

    public List<GraphicIntervalDTO> getOriginalGraphicIntervals() {
        return originalGraphicIntervals;
    }

    public void setOriginalGraphicIntervals(List<GraphicIntervalDTO> originalGraphicIntervals) {
        this.originalGraphicIntervals = originalGraphicIntervals;
    }

    public List<GraphicIntervalDTO> getGraphicIntervalsPlusThreeHours() {
        return graphicIntervalsPlusThreeHours;
    }

    public void setGraphicIntervalsPlusThreeHours(List<GraphicIntervalDTO> graphicIntervalsPlusThreeHours) {
        this.graphicIntervalsPlusThreeHours = graphicIntervalsPlusThreeHours;
    }

    public List<GraphicIntervalDTO> getGraphicIntervalsPlusSixHours() {
        return graphicIntervalsPlusSixHours;
    }

    public void setGraphicIntervalsPlusSixHours(List<GraphicIntervalDTO> graphicIntervalsPlusSixHours) {
        this.graphicIntervalsPlusSixHours = graphicIntervalsPlusSixHours;
    }

    public void onGraphicSelectInGraphicsView(){
        generateGraphics(measures);
    }



}
