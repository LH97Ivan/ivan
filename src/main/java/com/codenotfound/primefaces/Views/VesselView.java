package com.codenotfound.primefaces.Views;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Services.MeasureDetailService;
import com.codenotfound.primefaces.Services.VesselService;
import com.codenotfound.primefaces.DataDTO.VesselDTO;
import com.codenotfound.primefaces.Data.VesselData;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.MapModel;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("dtVesselView")
@ViewScoped
public class VesselView implements Serializable {

    private List<VesselDTO> vessels;

    private VesselDTO selectedVessel;

    private String selectedImo;

    private List<String> scaleStatusList = new ArrayList<>();

    public List<VesselDTO> getVessels(){
        return vessels;
    }

    public VesselDTO getSelectedVessel() {
        return selectedVessel;
    }

    public void setSelectedVessel(VesselDTO selectedVessel) {
        this.selectedVessel = selectedVessel;
    }

    public String getSelectedImo() {
        return selectedImo;
    }

    public void setSelectedImo(String selectedImo) {
        this.selectedImo = selectedImo;
    }

    public List<String> getScaleStatusList() {
        return scaleStatusList;
    }

    public void setScaleStatusList(List<String> scaleStatusList) {
        this.scaleStatusList = scaleStatusList;
    }

    @Inject
    VesselService vesselService;

    @PostConstruct
    public void init(){
        vessels = new ArrayList<>();
        refreshVessels();
        scaleStatusList.add("Planificada");
        scaleStatusList.add("En operacion");
        scaleStatusList.add("Finalizada");
        selectedVessel = new VesselDTO();
    }


    public void refreshVessels(){
        List<VesselData> vesselsData = vesselService.findAllVessels();
        vessels.clear();
        for(VesselData vesselData : vesselsData){
            VesselDTO vessel = vesselService.getVesselDTOFromData(vesselData);
            vessels.add(vessel);
        }
    }

}
