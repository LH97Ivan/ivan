package com.codenotfound.primefaces.Views;

import com.codenotfound.primefaces.Services.VesselService;
import com.codenotfound.primefaces.DataDTO.VesselDTO;
import com.codenotfound.primefaces.Data.VesselData;
import org.primefaces.model.map.*;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class MapView implements Serializable {

    @Inject
    VesselService vesselService;


    private MapModel mapModel;

    private String centerRevGeoMap;

    private Double posX;
    private Double posY;

    public MapModel getMapModel() {
        return mapModel;
    }

    public String getCenterRevGeoMap() {
        return centerRevGeoMap;
    }

    public void setCenterRevGeoMap(String centerRevGeoMap) {
        this.centerRevGeoMap = centerRevGeoMap;
    }

    public Double getPosX() {
        return posX;
    }

    public void setPosX(Double posX) {
        this.posX = posX;
    }

    public Double getPosY() {
        return posY;
    }

    public void setPosY(Double posY) {
        this.posY = posY;
    }



    @PostConstruct
    public void init() {

        mapModel = new DefaultMapModel();
        refreshMap(null);
        centerRevGeoMap = "36.140622, -5.396004";

    }

    public void refreshMap(VesselDTO selected){

        mapModel.getCircles().clear();
        mapModel.getPolylines().clear();

        Double cos;
        Double sin;
        Double posXHeading;
        Double posYHeading;


        List<VesselData> buquesData = vesselService.findAllVessels();



        for(VesselData vessel: buquesData){
            if (vessel.getPosX()!=null && vessel.getPosY()!=null){
                LatLng center = new LatLng(vessel.getPosX(), vessel.getPosY());

                Circle circle = new Circle(center, 150);
                if(selected!=null){
                    if(vessel.getId().equals(selected.getId())){
                        circle.setStrokeColor("#1D5B0F");
                        circle.setFillColor("#35EC0D");
                    } else{
                        circle.setStrokeColor("#d93c3c");
                        circle.setFillColor("#d93c3c");
                    }
                }else{
                    circle.setStrokeColor("#d93c3c");
                    circle.setFillColor("#d93c3c");
                }
                circle.setFillOpacity(0.5);
                circle.setId(vessel.getId().toString());
                mapModel.addOverlay(circle);

                if(vessel.getHeading()!=null){
                    Double heading = vessel.getHeading()%360;

                    cos = Math.cos(Math.toRadians(heading));
                    sin = Math.sin(Math.toRadians(heading));

                    posXHeading = vessel.getPosX()+cos*0.003;
                    posYHeading = vessel.getPosY()+sin*0.003;

                    LatLng posHeading = new LatLng(posXHeading, posYHeading);

                    Polyline polyline = new Polyline();
                    polyline.getPaths().add(center);
                    polyline.getPaths().add(posHeading);

                    polyline.setStrokeWeight(5);
                    polyline.setStrokeColor("#FF9900");
                    polyline.setStrokeOpacity(0.7);

                    mapModel.addOverlay(polyline);
                }
            }

        }
    }




    public void onReverseGeocode() {
        centerRevGeoMap = posX + "," + posY;
    }
}