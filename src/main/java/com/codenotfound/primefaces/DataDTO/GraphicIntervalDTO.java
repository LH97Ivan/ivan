package com.codenotfound.primefaces.DataDTO;

import com.codenotfound.primefaces.Data.GraphicInterval;

import java.io.Serializable;

public class GraphicIntervalDTO implements Serializable {

    private Long id;

    private String name;

    private int applyETA;

    private int initTime;

    private int endTime;

    private String status;

    public GraphicIntervalDTO(){}

    public GraphicIntervalDTO(Long id, String name, int applyETA, int initTime, int endTime, String status){
        this.id = id;
        this.name = name;
        this.applyETA = applyETA;
        this.initTime = initTime;
        this.endTime = endTime;
        this.status = status;
    }

    public GraphicIntervalDTO(GraphicInterval graphicInterval){
        this.id = graphicInterval.getId();
        this.name = graphicInterval.getName();
        this.applyETA = graphicInterval.getApplyETA();
        this.initTime = graphicInterval.getInitTime();
        this.endTime = graphicInterval.getEndTime();
        this.status = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getApplyETA() {
        return applyETA;
    }

    public void setApplyETA(int applyETA) {
        this.applyETA = applyETA;
    }

    public int getInitTime() {
        return initTime;
    }

    public void setInitTime(int initTime) {
        this.initTime = initTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return "Graphic Interval: " + initTime + " - " + endTime + ". Apply ETA: " + applyETA;
    }
}
