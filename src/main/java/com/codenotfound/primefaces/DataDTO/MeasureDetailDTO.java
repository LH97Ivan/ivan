package com.codenotfound.primefaces.DataDTO;

import com.codenotfound.primefaces.Data.MeasureDetail;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MeasureDetailDTO implements Serializable {
    private Long id;

    private String measureTypeName;

    private String measureIntervalName;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Double threshold1;

    private Double threshold2;

    private LocalDateTime eta;

    private LocalDateTime etd;

    private String statusDTO;

    public MeasureDetailDTO(){}

    public MeasureDetailDTO(MeasureDetailDTO measureDetailDTO){
        this.id = measureDetailDTO.getId();
        this.measureTypeName = measureDetailDTO.getMeasureTypeName();
        this.measureIntervalName = measureDetailDTO.getMeasureIntervalName();
        this.startTime = measureDetailDTO.getStartTime();
        this.endTime = measureDetailDTO.getEndTime();
        this.threshold1 = measureDetailDTO.getThreshold1();
        this.threshold2 = measureDetailDTO.getThreshold2();
        this.eta = measureDetailDTO.getEta();
        this.etd = measureDetailDTO.getEtd();
        this.statusDTO = measureDetailDTO.getStatusDTO();
    }

    public MeasureDetailDTO(MeasureDetail measureDetail){
        this.id = measureDetail.getId();
        this.measureTypeName = measureDetail.getMeasureTypeData().getName();
        this.measureIntervalName = measureDetail.getGraphicInterval().getName();
        this.threshold1 = measureDetail.getThreshold1();
        this.threshold2 = measureDetail.getThreshold2();
        this.eta = measureDetail.getVessel().getEta();
        this.etd = measureDetail.getVessel().getEtd();

        if(measureDetail.getGraphicInterval().getApplyETA()==1){
            this.startTime = measureDetail.getVessel().getEta().plusMinutes(measureDetail.getGraphicInterval().getInitTime());
            this.endTime = measureDetail.getVessel().getEta().plusMinutes(measureDetail.getGraphicInterval().getEndTime());
        } else if(measureDetail.getGraphicInterval().getApplyETA()==0){
            this.startTime = measureDetail.getVessel().getEta().plusMinutes(measureDetail.getGraphicInterval().getInitTime());
            this.endTime = measureDetail.getVessel().getEtd().plusMinutes(measureDetail.getGraphicInterval().getEndTime());
        } else if(measureDetail.getGraphicInterval().getApplyETA()==2){
            this.startTime = measureDetail.getVessel().getEtd().plusMinutes(measureDetail.getGraphicInterval().getInitTime());
            this.endTime = measureDetail.getVessel().getEtd().plusMinutes(measureDetail.getGraphicInterval().getEndTime());
        }
    }

    public MeasureDetailDTO(MeasureDetail measureDetail, LocalDateTime eta, LocalDateTime etd){
        this.id = measureDetail.getId();
        this.measureTypeName = measureDetail.getMeasureTypeData().getName();
        this.measureIntervalName = measureDetail.getGraphicInterval().getName();
        this.threshold1 = measureDetail.getThreshold1();
        this.threshold2 = measureDetail.getThreshold2();
        this.eta = eta;
        this.etd = etd;

        if(measureDetail.getGraphicInterval().getApplyETA()==1){
            this.startTime = eta.plusMinutes(measureDetail.getGraphicInterval().getInitTime());
            this.endTime = eta.plusMinutes(measureDetail.getGraphicInterval().getEndTime());
        } else if(measureDetail.getGraphicInterval().getApplyETA()==0){
            this.startTime = eta.plusMinutes(measureDetail.getGraphicInterval().getInitTime());
            this.endTime = etd.plusMinutes(measureDetail.getGraphicInterval().getEndTime());
        } else if(measureDetail.getGraphicInterval().getApplyETA()==2){
            this.startTime = etd.plusMinutes(measureDetail.getGraphicInterval().getInitTime());
            this.endTime = etd.plusMinutes(measureDetail.getGraphicInterval().getEndTime());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMeasureTypeName() {
        return measureTypeName;
    }

    public void setMeasureTypeName(String measureTypeName) {
        this.measureTypeName = measureTypeName;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getThreshold1() {
        return threshold1;
    }

    public void setThreshold1(Double threshold1) {
        this.threshold1 = threshold1;
    }

    public Double getThreshold2() {
        return threshold2;
    }

    public void setThreshold2(Double threshold2) {
        this.threshold2 = threshold2;
    }

    public String getStatusDTO() {
        return statusDTO;
    }

    public void setStatusDTO(String statusDTO) {
        this.statusDTO = statusDTO;
    }

    public String getMeasureIntervalName() {
        return measureIntervalName;
    }

    public void setMeasureIntervalName(String measureIntervalName) {
        this.measureIntervalName = measureIntervalName;
    }

    public LocalDateTime getEta() {
        return eta;
    }

    public void setEta(LocalDateTime eta) {
        this.eta = eta;
    }

    public LocalDateTime getEtd() {
        return etd;
    }

    public void setEtd(LocalDateTime etd) {
        this.etd = etd;
    }

    @Override
    public String toString(){
        return "Measure Type: " + measureTypeName;
    }
}
