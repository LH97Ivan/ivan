package com.codenotfound.primefaces.DataDTO;

import com.codenotfound.primefaces.Data.MeasureTypeData;
import com.codenotfound.primefaces.Data.VesselData;
import com.codenotfound.primefaces.Enums.ScaleStatus;
import com.codenotfound.primefaces.Services.VesselService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class VesselDTO implements Serializable {

    private Long id;

    private String name;

    private String imo;

    private Double posX;

    private Double posY;

    private Double heading;

    private LocalDateTime eta;

    private LocalDateTime etd;

    private String generalStatus;

    private String scaleStatus;

    private String dock;

    private String newETA = "";

    private String newETD = "";

    public VesselDTO() {}

    public VesselDTO(Long id, String name, String imo, Double posX, Double posY, Double heading, LocalDateTime eta, LocalDateTime etd, String generalStatus, String scaleStatus, String dock, String newETA, String newETD) {
        this.id = id;
        this.name = name;
        this.imo = imo;
        this.posX = posX;
        this.posY = posY;
        this.heading = heading;
        this.eta = eta;
        this.etd = etd;
        this.generalStatus = generalStatus;
        this.scaleStatus = scaleStatus;
        this.dock = dock;
        this.newETA = newETA;
        this.newETD = newETD;
    }

    public VesselDTO(VesselData vesselData){
        this.id = vesselData.getId();
        this.name = vesselData.getName();
        this.imo = vesselData.getImo();
        this.posX = vesselData.getPosX();
        this.posY = vesselData.getPosY();
        this.heading = vesselData.getHeading();
        this.eta = vesselData.getEta();
        this.etd = vesselData.getEtd();
        this.scaleStatus = vesselData.getScaleStatus();
        this.dock = vesselData.getDock();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImo() {
        return imo;
    }

    public void setImo(String imo) {
        this.imo = imo;
    }

    public Double getPosX() {
        return posX;
    }

    public void setPosX(Double posX) {
        this.posX = posX;
    }

    public Double getPosY() {
        return posY;
    }

    public void setPosY(Double posY) {
        this.posY = posY;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public LocalDateTime getEta() {
        return eta;
    }

    public void setEta(LocalDateTime eta) {
        this.eta = eta;
    }

    public LocalDateTime getEtd() {
        return etd;
    }

    public void setEtd(LocalDateTime etd) {
        this.etd = etd;
    }

    public String getGeneralStatus() {
        return generalStatus;
    }

    public void setGeneralStatus(String generalStatus) {
        this.generalStatus = generalStatus;
    }

    public String getScaleStatus() {
        return scaleStatus;
    }

    public void setScaleStatus(String scaleStatus) {
        this.scaleStatus = scaleStatus;
    }

    public String getDock() {
        return dock;
    }

    public void setDock(String dock) {
        this.dock = dock;
    }

    public String getNewETA() {
        return newETA;
    }

    public void setNewETA(String newETA) {
        this.newETA = newETA;
    }

    public String getNewETD() {
        return newETD;
    }

    public void setNewETD(String newETD) {
        this.newETD = newETD;
    }

    @Override
    public java.lang.String toString() {
        return "Vessel{ Id: " + id + " name: " + name + " imo: " + imo +  " posX:" + posX + " posY:" + posY + " heading:" + heading + "}";
    }
}
