package com.codenotfound.primefaces.DataDTO;

import com.codenotfound.primefaces.Data.MeasureTypeData;

import javax.persistence.Id;
import java.io.Serializable;

public class MeasureTypeDTO implements Serializable {

    @Id
    private Long id;

    private String name;

    public MeasureTypeDTO(){
    }

    public MeasureTypeDTO(Long id, String name){
        this.id = id;
        this.name = name;
    }

    public MeasureTypeDTO(MeasureTypeData measureTypeData){
        this.id = measureTypeData.getId();
        this.name = measureTypeData.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "Measure type: " + name;
    }
}
