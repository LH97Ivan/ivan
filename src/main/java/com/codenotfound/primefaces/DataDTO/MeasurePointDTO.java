package com.codenotfound.primefaces.DataDTO;

import com.codenotfound.primefaces.Data.MeasurePoint;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MeasurePointDTO implements Serializable {

    private Long id;

    private Double value;

    private LocalDateTime time;


    public MeasurePointDTO(){}

    public MeasurePointDTO(MeasurePoint measurePoint){
        this.id = measurePoint.getId();
        this.time = measurePoint.getTime();
        this.value = measurePoint.getValue();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString(){
        return "Point ID: " + id;
    }
}
