package com.codenotfound.primefaces.Services;

import com.codenotfound.primefaces.Data.MeasureData;
import com.codenotfound.primefaces.Repository.MeasureDataRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class MeasureDataService {

    @Autowired
    MeasureDataRepository measureDataRepository;

    public List<MeasureData> findAll(){
        List<MeasureData> list = measureDataRepository.findAll();
        return list;
    }

    public MeasureData findOne(Long id){
        MeasureData measureData = measureDataRepository.findById(id).get();
        return measureData;
    }

    public void addMeasureData(MeasureData measureData) {
        measureDataRepository.save(measureData);
    }

    public void delete(MeasureData measureData) {
        measureDataRepository.delete(measureData);
    }
}
