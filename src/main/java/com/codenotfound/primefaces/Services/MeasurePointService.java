package com.codenotfound.primefaces.Services;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Data.MeasurePoint;
import com.codenotfound.primefaces.Data.MeasureTypeData;
import com.codenotfound.primefaces.DataDTO.MeasureDetailDTO;
import com.codenotfound.primefaces.DataDTO.MeasurePointDTO;
import com.codenotfound.primefaces.DataDTO.MeasureTypeDTO;
import com.codenotfound.primefaces.Repository.MeasureDetailRepository;
import com.codenotfound.primefaces.Repository.MeasurePointRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class MeasurePointService {

    @Autowired
    MeasurePointRepository measurePointRepository;

    @Autowired
    MeasureTypeService measureTypeService;

    @Autowired
    MeasureDetailRepository measureDetailRepository;


    public List<MeasurePointDTO> findAllPointsByMeasureId(Long id) {
        List<MeasurePointDTO> points = new ArrayList<>();
        List<MeasurePoint> allPoints = measurePointRepository.findAll();
        MeasureDetail measureDetailDTO = measureDetailRepository.findById(id).get();
        for(MeasurePoint point : allPoints){
            if(point.getMeasureData().getId().equals(measureDetailDTO.getMeasureData().getId())){
                MeasurePointDTO pointDTO = new MeasurePointDTO(point);
                points.add(pointDTO);
            }
        }
        return points;
    }

    public List<MeasurePoint> findAll() {
        return measurePointRepository.findAll();
    }

    public MeasurePoint findOne(Long id) {
        return measurePointRepository.findById(id).get();
    }

    public void addMeasurePoint(MeasurePoint measurePoint) {
        measurePointRepository.save(measurePoint);
    }

    public void delete(MeasurePoint measurePoint) {
        measurePointRepository.delete(measurePoint);
    }

    public List<MeasurePointDTO> findPointsBetweenStartAndEndTime(LocalDateTime startTime, LocalDateTime endTime, Long measureId) {
        List<MeasurePointDTO> pointsToCheck = findAllPointsByMeasureId(measureId);
        List<MeasurePointDTO> res = new ArrayList<>();
        for(MeasurePointDTO point: pointsToCheck){
            if(point.getTime().isBefore(endTime) && point.getTime().isAfter(startTime)){
                 res.add(point);
            }
        }
        return res;
    }

    public void deleteMeasurePointsByMeasureDataId(Long id) {
        for(MeasurePoint measurePoint: measurePointRepository.findAll()){
            if(measurePoint.getMeasureData().getId().equals(id)){
                measurePointRepository.deleteById(measurePoint.getId());
            }
        }
    }
}
