package com.codenotfound.primefaces.Services;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Data.VesselData;
import com.codenotfound.primefaces.DataDTO.MeasureDetailDTO;
import com.codenotfound.primefaces.DataDTO.VesselDTO;
import com.codenotfound.primefaces.Repository.VesselRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class VesselService {

    @Autowired
    VesselRepository vesselRepository;

    @Autowired
    MeasureDetailService measureDetailService;

    public List<VesselData> findAllVessels() {
        List<VesselData> list = vesselRepository.findAll();
        return list;
    }


    public void addVessel(VesselData buque){
        vesselRepository.save(buque);
    }

    public VesselData findOne(Long id) {
        return vesselRepository.findById(id).get();
    }

    public void updateVessel(VesselDTO buque1) {
        VesselData buque2 = vesselRepository.findById(buque1.getId()).get();
        buque2.setImo(buque1.getImo());
        buque2.setName(buque1.getName());
        buque2.setPosX(buque1.getPosX());
        buque2.setPosY(buque1.getPosY());
        buque2.setHeading(buque1.getHeading());
        vesselRepository.save(buque2);
    }

    public void delete(VesselData vessel) {
        vesselRepository.delete(vessel);
    }


    public VesselDTO findVesselByImo(String imo){
        VesselDTO vessel = new VesselDTO();
        for(VesselData vesselData: vesselRepository.findAll()){
            if(vesselData.getImo().equals(imo)){
                vessel = getVesselDTOFromData(vesselData);
                break;
            }
        }
        return vessel;
    }

    public VesselDTO getVesselDTOFromData(VesselData vesselData){
        VesselDTO vesselDTO = new VesselDTO(vesselData);
        for(MeasureDetail measureDetail: measureDetailService.findMeasuresByVesselId(vesselData.getId())){
            MeasureDetailDTO measureDetailDTO = measureDetailService.getMeasureDetailDTOFromData(measureDetail);
            if(measureDetailDTO.getStatusDTO()!=null){
                if(measureDetailDTO.getStatusDTO().equals("Red")){
                    vesselDTO.setGeneralStatus("Red");
                    break;
                } else if(measureDetailDTO.getStatusDTO().equals("Yellow")){
                    vesselDTO.setGeneralStatus("Yellow");
                } else if(vesselDTO.getGeneralStatus()==null || !vesselDTO.getGeneralStatus().equals("Yellow")){
                    vesselDTO.setGeneralStatus("Green");
                }
            }
        }
        return vesselDTO;
    }



}