package com.codenotfound.primefaces.Services;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Data.MeasureTypeData;
import com.codenotfound.primefaces.Repository.MeasureDetailRepository;
import com.codenotfound.primefaces.Repository.MeasureTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class MeasureTypeService {

    @Autowired
    MeasureTypeRepository measureTypeRepository;

    @Autowired
    MeasureDetailRepository measureDetailRepository;

    public List<MeasureTypeData> findAllMeasures(){
        List<MeasureTypeData> list = measureTypeRepository.findAll();
        return list;
    }

    public MeasureTypeData findOneMeasureById(Long id){
        return measureTypeRepository.findById(id).get();
    }


}
