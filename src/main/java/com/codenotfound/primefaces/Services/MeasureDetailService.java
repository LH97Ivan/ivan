package com.codenotfound.primefaces.Services;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.DataDTO.MeasureDetailDTO;
import com.codenotfound.primefaces.DataDTO.MeasurePointDTO;
import com.codenotfound.primefaces.Repository.MeasureDetailRepository;
import com.codenotfound.primefaces.Repository.MeasureTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class MeasureDetailService {

    @Autowired
    MeasureDetailRepository measureDetailRepository;

    @Autowired
    MeasureTypeRepository measureTypeRepository;

    @Autowired
    MeasurePointService measurePointService;

    public MeasureDetail findById(long id){
        return measureDetailRepository.findById(id).get();
    }

    public void deleteMeasuresDetailByVesselId(Long id){
        for(MeasureDetail measureDetail: measureDetailRepository.findAll()){
            if(measureDetail.getVessel().getId().equals(id)){
                measureDetailRepository.deleteById(measureDetail.getId());
            }
        }
    }

    public void deleteMeasuresDetailByMeasureDataId(Long id) {
        for(MeasureDetail measureDetail: measureDetailRepository.findAll()){
            if(measureDetail.getMeasureData().getId().equals(id)){
                measureDetailRepository.deleteById(measureDetail.getId());
            }
        }
    }


    public List<MeasureDetail> findAll() {
        List<MeasureDetail> measureDetails = measureDetailRepository.findAll();
        return measureDetails;
    }

    public List<MeasureDetail> findMeasuresByVesselId(Long id){
        List<MeasureDetail> measures = new ArrayList<>();
        for(MeasureDetail measureDetail: measureDetailRepository.findAll()){
            if(measureDetail.getVessel().getId().equals(id)){
                measures.add(measureDetail);
            }
        }
        return measures;
    }

    public MeasureDetail findOne(Long id) {
        return measureDetailRepository.findById(id).get();
    }

    public void addMeasure(MeasureDetail measureDetail) {
        measureTypeRepository.save(measureDetail.getMeasureTypeData());
        measureDetailRepository.save(measureDetail);
    }

    public void delete(MeasureDetail measureDetail) {
        measureDetailRepository.delete(measureDetail);
    }

    public MeasureDetailDTO getMeasureDetailDTOFromData(MeasureDetail measureDetail)
    {
        MeasureDetailDTO measureDetailDTO = new MeasureDetailDTO(measureDetail);
            List<MeasurePointDTO> pointsBetweenTimes = measurePointService.findPointsBetweenStartAndEndTime(measureDetailDTO.getStartTime(), measureDetailDTO.getEndTime(), measureDetailDTO.getId());
            for(MeasurePointDTO point: pointsBetweenTimes){
                if(point.getValue()>=measureDetailDTO.getThreshold2()){
                    measureDetailDTO.setStatusDTO("Red");
                    break;
                } else if(point.getValue()>=measureDetailDTO.getThreshold1()){
                    measureDetailDTO.setStatusDTO("Yellow");
                } else if(measureDetailDTO.getStatusDTO()==null || !measureDetailDTO.getStatusDTO().equals("Yellow")){
                    measureDetailDTO.setStatusDTO("Green");
                }
            }
            return measureDetailDTO;
    }

    public MeasureDetailDTO getMeasureDetailDTOFromDataChangingETAAndETD(MeasureDetail measureDetail, LocalDateTime eta, LocalDateTime etd)
    {
        MeasureDetailDTO measureDetailDTO = new MeasureDetailDTO(measureDetail, eta, etd);
        List<MeasurePointDTO> pointsBetweenTimes = measurePointService.findPointsBetweenStartAndEndTime(measureDetailDTO.getStartTime(), measureDetailDTO.getEndTime(), measureDetailDTO.getId());
        for(MeasurePointDTO point: pointsBetweenTimes){
            if(point.getValue()>=measureDetailDTO.getThreshold2()){
                measureDetailDTO.setStatusDTO("Red");
                break;
            } else if(point.getValue()>=measureDetailDTO.getThreshold1()){
                measureDetailDTO.setStatusDTO("Yellow");
            } else if(measureDetailDTO.getStatusDTO()==null || !measureDetailDTO.getStatusDTO().equals("Yellow")){
                measureDetailDTO.setStatusDTO("Green");
            }
        }
        return measureDetailDTO;
    }


}
