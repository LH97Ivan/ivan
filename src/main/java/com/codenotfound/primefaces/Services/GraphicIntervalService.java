package com.codenotfound.primefaces.Services;

import com.codenotfound.primefaces.Data.GraphicInterval;
import com.codenotfound.primefaces.Repository.GraphicIntervalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class GraphicIntervalService {

    @Autowired
    GraphicIntervalRepository graphicIntervalRepository;

    public List<GraphicInterval> findAll(){
        List<GraphicInterval> list = graphicIntervalRepository.findAll();
        return list;
    }

    public GraphicInterval findOne(Long id){
        return graphicIntervalRepository.findById(id).get();
    }

    public void delete(GraphicInterval graphicInterval){
        graphicIntervalRepository.delete(graphicInterval);
    }

    public GraphicInterval findByName(String name){
        for(GraphicInterval graphicInterval: graphicIntervalRepository.findAll()){
            if(graphicInterval.getName().equals(name)){
                return graphicInterval;
            }
        }
        return null;
    }
}
