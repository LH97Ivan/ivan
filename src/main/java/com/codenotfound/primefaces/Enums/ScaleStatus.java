package com.codenotfound.primefaces.Enums;

import java.io.Serializable;

public enum ScaleStatus implements Serializable {

    PLANIFICADA, EN_OPERACION, FINALIZADA
}
