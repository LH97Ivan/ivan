package com.codenotfound.primefaces.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MeasureDetail implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private VesselData vessel;

    @ManyToOne
    private MeasureTypeData measureTypeData;

    @ManyToOne
    private GraphicInterval graphicInterval;

    @ManyToOne
    private MeasureData measureData;

    private Double threshold1;

    private Double threshold2;

    private String statusData;


    public MeasureDetail(){}

    public MeasureDetail(Long id, Double threshold1, Double threshold2, String statusData){
        this.id = id;
        this.threshold1 = threshold1;
        this.threshold2 = threshold2;
        this.statusData = statusData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VesselData getVessel() {
        return vessel;
    }

    public void setVessel(VesselData vessel) {
        this.vessel = vessel;
    }

    public MeasureTypeData getMeasureTypeData() {
        return measureTypeData;
    }

    public void setMeasureTypeData(MeasureTypeData measureTypeData) {
        this.measureTypeData = measureTypeData;
    }

    public GraphicInterval getGraphicInterval() {
        return graphicInterval;
    }

    public void setGraphicInterval(GraphicInterval graphicInterval) {
        this.graphicInterval = graphicInterval;
    }

    public Double getThreshold1() {
        return threshold1;
    }

    public void setThreshold1(Double threshold1) {
        this.threshold1 = threshold1;
    }

    public Double getThreshold2() {
        return threshold2;
    }

    public void setThreshold2(Double threshold2) {
        this.threshold2 = threshold2;
    }

    public String getStatusData() {
        return statusData;
    }

    public void setStatusData(String statusData) {
        this.statusData = statusData;
    }

    public MeasureData getMeasureData() {
        return measureData;
    }

    public void setMeasureData(MeasureData measureData) {
        this.measureData = measureData;
    }
}
