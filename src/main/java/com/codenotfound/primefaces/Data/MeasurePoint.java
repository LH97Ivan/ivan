package com.codenotfound.primefaces.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class MeasurePoint implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private Double value;

    private LocalDateTime time;

    @ManyToOne
    private MeasureData measureData;

    public MeasurePoint(){}

    public MeasurePoint(Long id, Double value, LocalDateTime time){
        this.id = id;
        this.value = value;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public MeasureData getMeasureData() {
        return measureData;
    }

    public void setMeasureData(MeasureData measureData) {
        this.measureData = measureData;
    }
}
