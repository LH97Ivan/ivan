package com.codenotfound.primefaces.Data;


import com.codenotfound.primefaces.Enums.ScaleStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class VesselData implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String imo;

    private Double posX;

    private Double posY;

    private Double heading;

    private LocalDateTime eta;

    private LocalDateTime etd;

    private String scaleStatus;

    private String dock;

    public VesselData() {}

    public VesselData(Long id, String name, String imo, Double posX, Double posY, Double heading, LocalDateTime eta, LocalDateTime etd, String scaleStatus, String dock) {
        this.id = id;
        this.name = name;
        this.imo = imo;
        this.posX = posX;
        this.posY = posY;
        this.heading = heading;
        this.eta = eta;
        this.etd = etd;
        this.scaleStatus = scaleStatus;
        this.dock = dock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImo() {
        return imo;
    }

    public void setImo(String imo) {
        this.imo = imo;
    }

    public Double getPosX() {
        return posX;
    }

    public void setPosX(Double posX) {
        this.posX = posX;
    }

    public Double getPosY() {
        return posY;
    }

    public void setPosY(Double posY) {
        this.posY = posY;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public LocalDateTime getEta() {
        return eta;
    }

    public void setEta(LocalDateTime eta) {
        this.eta = eta;
    }

    public LocalDateTime getEtd() {
        return etd;
    }

    public void setEtd(LocalDateTime etd) {
        this.etd = etd;
    }

    public String getScaleStatus() {
        return scaleStatus;
    }

    public void setScaleStatus(String scaleStatus) {
        this.scaleStatus = scaleStatus;
    }

    public String getDock() {
        return dock;
    }

    public void setDock(String dock) {
        this.dock = dock;
    }

    @Override
    public java.lang.String toString() {
        return "Vessel{ Id: " + id + " name: " + name + " imo: " + imo +  " posX:" + posX + " posY:" + posY + " heading:" + heading + "}";
    }
}