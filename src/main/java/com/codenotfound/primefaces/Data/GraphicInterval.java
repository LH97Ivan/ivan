package com.codenotfound.primefaces.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class GraphicInterval implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int applyETA;

    private int initTime;

    private int endTime;

    public GraphicInterval(){}

    public GraphicInterval(Long id, String name, int applyETA, int initTime, int endTime){
        this.id = id;
        this.name = name;
        this.applyETA = applyETA;
        this.initTime = initTime;
        this.endTime = endTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getApplyETA() {
        return applyETA;
    }

    public void setApplyETA(int applyETA) {
        this.applyETA = applyETA;
    }

    public int getInitTime() {
        return initTime;
    }

    public void setInitTime(int initTime) {
        this.initTime = initTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString(){
        return "Graphic Interval: " + initTime + " - " + endTime + ". Apply ETA: " + applyETA;
    }
}
