package com.codenotfound.primefaces.Repository;

import com.codenotfound.primefaces.Data.MeasureDetail;
import com.codenotfound.primefaces.Data.MeasureTypeData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeasureDetailRepository extends JpaRepository<MeasureDetail, Long> {
}
