package com.codenotfound.primefaces.Repository;

import com.codenotfound.primefaces.Data.VesselData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VesselRepository extends JpaRepository<VesselData, Long> {


}
