package com.codenotfound.primefaces.Repository;

import com.codenotfound.primefaces.Data.MeasurePoint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeasurePointRepository extends JpaRepository<MeasurePoint, Long> {
}
