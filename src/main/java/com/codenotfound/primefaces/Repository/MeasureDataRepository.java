package com.codenotfound.primefaces.Repository;

import com.codenotfound.primefaces.Data.MeasureData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeasureDataRepository extends JpaRepository<MeasureData, Long> {
}
