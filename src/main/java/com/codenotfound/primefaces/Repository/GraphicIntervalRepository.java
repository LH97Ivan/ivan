package com.codenotfound.primefaces.Repository;

import com.codenotfound.primefaces.Data.GraphicInterval;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GraphicIntervalRepository extends JpaRepository<GraphicInterval, Long> {
}
