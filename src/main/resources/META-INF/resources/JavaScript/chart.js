

function createChart(chartName, values, times, startTime, endTime, threshold1, threshold2, eta, etd, intervalColor)
{

    var options = {};
    var color2apply="";
    if(intervalColor=="Red"){
        color2apply="#e6281a";
    } else if(intervalColor=="Green"){
        color2apply="#1fe432";
    } else if(intervalColor=="Yellow"){
        color2apply="#f7f028";
    }
    if (values.length==0 || times==''){
        alert("There's no graphic selected")
    } else{
        options = {
                    series: [{
                      data: values
                    }],
                    chart: {
                      height: 350,
                      type: 'line',
                      id: 'areachart-2'
                    },
                    annotations: {
                      yaxis: [{
                        y: threshold1,
                        borderColor: '#F0F713',
                        label: {
                            borderColor: '#F0F713',
                            style: {
                                color: '#000000',
                                background: '#F0F713',
                            },
                            text: threshold1,
                        }
                      },{
                        y: threshold2,
                        borderColor: '#F50505',
                        label: {
                            borderColor: '#F50505',
                            style: {
                                color: '#000000',
                                background: '#F50505',
                            },
                            text: threshold2,
                        }
                      }],
                      xaxis: [{
                        x: new Date(eta).getTime(),
                        strokeDashArray: 0,
                        borderColor: '#775DD0',
                        label: {
                            borderColor: '#775DD0',
                            style: {
                                color: '#fff',
                                background: '#775DD0',
                            },
                            text: 'ETA',
                        }
                      }, {
                        x: new Date(etd).getTime(),
                        strokeDashArray: 0,
                        borderColor: '#775DD0',
                        label: {
                            borderColor: '#775DD0',
                            style: {
                                color: '#fff',
                                background: '#775DD0',
                            },
                            text: 'ETD',
                        }
                      },  {
                        x: new Date(startTime).getTime(),
                        x2: new Date(endTime).getTime(),
                        fillColor: color2apply,
                        opacity: 0.4,
                        label: {
                          borderColor: '#B3F7CA',
                          style: {
                            fontSize: '10px',
                            color: '#fff',
                            background: color2apply,
                          },
                          offsetY: -10,
                          text: 'Interval',
                        }
                      }],
                    },
                    tooltip: {
                    	enabled: true
                    },
                    dataLabels: {
                      enabled: false
                    },
                    stroke: {
                      curve: 'straight'
                    },
                    grid: {
                      padding: {
                        right: 30,
                        left: 20
                      }
                    },
                    title: {
                      text: chartName,
                      align: 'left'
                    },
                    labels: times.split(","),
                                    xaxis: {
                      type: 'datetime',
                    },
                  };
    }



   /* chartTitle = options.title.text + " para el buque " + vesselImo;
    options.title.text = chartTitle;
    alert (chartTitle);*/
    var chart = new ApexCharts(document.querySelector("#" + chartName), options);
    chart.render();
};
